# ACIMOV Pre-Commit Hooks

See [`pre-commit` documentation](https://pre-commit.com/).

See also the [`acimov-methodology-template` Git project](https://gitlab.com/acimov/acimov-methodology-template).
