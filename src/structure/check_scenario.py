from __future__ import annotations

import argparse
import os
from typing import Sequence
import mistune
import rdflib

PASS = 0
FAIL = 1

def test_scenario_title(m):
    assert "name" in m, ("must", "The scenario has a title")

def test_scenario_desc(m):
    assert "description" in m, ("must", "The scenario has a description")

def test_scenario_cq(m):
    assert len(m["questions"]) > 0, ("must", "The scenario addresses at least one competency question")

def test_scenario_term(m):
    # TODO add check that the term is in onto.ttl
    assert len(m["terms"]) > 0, ("must", "The scenario introduces at least one term")

def test_scenario_cqs(m):
    assert len(m["questions"]) > 1, ("should", "The scenarion addresses more than one competecy question")

# TODO as HTML instead?
def get_text(ast):
    if "children" not in ast:
        # text or codespan
        return ast["text"]
    else:
        text = ""
        for child in ast["children"]:
            text += get_text(child)
        return text

def find_section(ast, title, level=2):
    for (i, item) in enumerate(ast):
        if item["type"] == "heading" \
        and item["level"] == level \
        and item["children"][0]["text"] == title \
        and len(ast) > i+1:
            return ast[i+1]
    return None

def parse_scenario(filename):
    name = os.path.basename(filename)
    path = os.path.dirname(filename)

    sc = {
        "id": name,
        "path": path,
        "name": None,
        "description": None,
        "questions": [],
        "terms": []
    }

    if not os.path.exists(filename):
        return sc

    get_ast = mistune.create_markdown(renderer='ast', plugins=['table'])
    md = open(filename, "r").read()
    ast = get_ast(md)

    # first element is the title of the scenario
    if ast[0]["type"] == "heading" and ast[0]["level"] == 1:
        sc["name"] = get_text(ast[0])

    # second element is the description of the scenario
    descItem = find_section(ast, "Description")
    if descItem is not None and descItem["type"] == "paragraph":
        sc["description"] = get_text(descItem)

    # next elements are competency questions
    qItem = find_section(ast, "Competency Questions")
    if qItem is not None and qItem["type"] == "table":
        for (i, item) in enumerate(qItem["children"][1]["children"]):
            if len(item["children"]) == 3:
                id = get_text(item["children"][0])
                sc["questions"].append(id)
            else:
                # FIXME generates an errore when opening None.rq...
                sc["questions"].append(None)

    # next element is the glossary
    glossaryItem = find_section(ast, "Glossary")
    if glossaryItem is not None and glossaryItem["type"] == "list":
        for item in glossaryItem["children"]:
            t = {}
            if len(item["children"]) == 1 and item["children"][0]["type"] == "block_text":
                t["label"] = get_text(item["children"][0]["children"][0])
            if len(item["children"][0]["children"]) > 0 and item["children"][0]["children"][0]["type"] == "link":
                t["ref"] = item["children"][0]["children"][0]["link"]
            sc["terms"].append(t)

    return sc

def check_scenario(filename):
    sc = parse_scenario(filename)

    try:
        test_scenario_title(sc)
        test_scenario_desc(sc)
        test_scenario_cq(sc)
        test_scenario_term(sc)
        test_scenario_cqs(cs)
    except AssertionError as e:
        print(f"Assertion not fulfilled in file {filename}:")
        print(e)
        return FAIL

    return PASS

def main(argv: Sequence[str] | None = None) -> int:
    parser = argparse.ArgumentParser()
    parser.add_argument('filenames', nargs='*', help='Filenames to fix')
    args = parser.parse_args(argv)

    retv = 0

    for filename in args.filenames:
        ret_for_file = check_scenario(filename)
        retv |= ret_for_file

    return retv

if __name__ == '__main__':
    raise SystemExit(main())
