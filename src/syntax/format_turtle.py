from __future__ import annotations

import argparse
import os
from typing import IO
from typing import Sequence

from rdflib import Graph

PASS = 0
FAIL = 1

def format_turtle(filename: str, content: str) -> int:
    g = Graph()
    try:
        g.parse(data=content)
    except Exception as e:
        print(f"Turtle error in {filename}. Passing.")
        return PASS
    
    after_string = g.serialize(format="turtle", base=g.base)
    
    if content == after_string:
        return PASS
    else:
        print(f'Formatting {filename}')
        with open(filename, 'w', encoding="UTF-8") as file_obj: 
            content = file_obj.write(after_string)
        return FAIL

def main(argv: Sequence[str] | None = None) -> int:
    parser = argparse.ArgumentParser()
    parser.add_argument('filenames', nargs='*', help='Filenames to fix')
    args = parser.parse_args(argv)

    retv = PASS

    for filename in args.filenames:
        with open(filename, encoding='UTF-8') as file_obj: 
            content = file_obj.read()

        ret_for_file = format_turtle(filename, content)
        retv |= ret_for_file
            
    return retv

if __name__ == '__main__':
    raise SystemExit(main())