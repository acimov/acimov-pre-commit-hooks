from __future__ import annotations

import argparse
from typing import IO
from typing import Sequence

from rdflib.plugins.sparql.parser import parseQuery

PASS = 0
FAIL = 1

def check_sparql(filename: IO[bytes]) -> int:
    with open(filename,'r') as file:
        query = file.read()

        try:
            parseQuery(query)
            return PASS
        except Exception as e:
            print("Syntax error on query:")
            print(query)
            print(e)
            return FAIL

def main(argv: Sequence[str] | None = None) -> int:
    parser = argparse.ArgumentParser()
    parser.add_argument('filenames', nargs='*', help='Filenames to fix')
    args = parser.parse_args(argv)

    retv = 0

    for filename in args.filenames:
        ret_for_file = check_sparql(filename)
        retv |= ret_for_file

    return retv

if __name__ == '__main__':
    raise SystemExit(main())