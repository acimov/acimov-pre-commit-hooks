from __future__ import annotations

import argparse
import os
from typing import IO
from typing import Sequence

from rdflib import Graph

PASS = 0
FAIL = 1

def check_turtle(file_obj: IO[bytes]) -> int:
    g = Graph()

    try:
        g.parse(file_obj)
        return PASS
    except Exception as e:
        print(f"Turtle error in {file_obj} " + str(e))
        return FAIL

def main(argv: Sequence[str] | None = None) -> int:
    parser = argparse.ArgumentParser()
    parser.add_argument('filenames', nargs='*', help='Filenames to fix')
    args = parser.parse_args(argv)

    retv = 0

    for filename in args.filenames:
        ret_for_file = check_turtle(filename)
        retv |= ret_for_file

    return retv

if __name__ == '__main__':
    raise SystemExit(main())